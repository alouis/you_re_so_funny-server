# You're so funny
This is the backend for the eponymous mobile app

### Server :globe_with_meridians: 
 * Node: open-source, cross-platform runtime environment that allows developers to create all kinds of server-side tools and applications in JavaScript
 * [Express](https://github.com/expressjs/express/tree/5.0): Node.js module that is a web framework providing mechanisms to write handlers for requests with different HTTP verbs at different URL paths (routes). [Introduction to Express/Node](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/Introduction).
 * [HTTP by Modzilla Doc](https://developer.mozilla.org/en-US/docs/Web/HTTP)

### Database :file_cabinet:
 * [PostgreSQL documentation](https://www.postgresql.org/docs/9.1/sql-altertable.html)
