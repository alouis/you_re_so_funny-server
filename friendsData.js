const data = [
    {
       id:181808,
       name:"Morgan",
       picture_path:"http://localhost:3000/img/Morgan.png",
       original_title:"The Boss",
       overview:"You've got troubles ? He's your man",
       release_date:"1988-01-31",
       sound_paths: [require('../../assets/Alle.m4a'),
         require('../../assets/Voix143.m4a'),
         require('../../assets/Pet0.m4a')]
    },
    {
       id:181809,
       name:"Sofiane",
      //  picture_path:require('./Sofiane.png'),
       original_title:"Pikachu",
       overview:"As bright as his favorite color\n",
       release_date:"1997-01-12",
       sound_paths: []
    },
    {
       id:181810,
       name:"Estelle",
      //  picture_path:require('./Estelle.png'),
       original_title:"La grande bleue",
       overview:"There will never be enough space to introduce such a star",
       release_date:"1994-12-20",
       sound_paths: []
    },
    {
       id:181811,
       name:"Alex",
      //  picture_path:require('./Alex.png'),
       original_title:"Original Sacha",
       overview:"Same same\n",
       release_date:"1995-06-20",
       sound_paths: []
    },
    {
       id:181812,
       name:"Lucas",
      //  picture_path:require('./Lucas.png'),
       original_title:"Lalala",
       overview:"Hihihi\n",
       release_date:"1991-12-02",
       sound_paths: []
    },
    {
       id:181813,
       name:"Andrea",
      //  picture_path:require('./Andrea.png'),
       original_title:"Me",
       overview:"Trying my best\n",
       release_date:"1994-05-14",
       sound_paths: []
    }
 ]

 module.exports = data;
 