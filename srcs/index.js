const express = require('express');
// const friendsData = require('./public/data/friendsData') -> friendsData in Database
const client = require('./postgres')

const app = express();
app.get('/', (req, res) => {
    res.status(200).send(
        `
        <html>Hello world!</html>
        `
    );
})

app.get('/test', (req, res) => {
    res.status(200).send(
        `
        <html>Hello ${req.query.name.toUpperCase()}</html>
        `
    );
})

app.get('/backgroundImg', (req, res) => {
    res.status(200).send("http://pngimg.com/uploads/mars_planet/mars_planet_PNG23.png")
})

app.get('/friendsData', async (req, res) => {
    const ret = await client.query ('SELECT * FROM friends')
    console.log(ret)
    res.status(200).send(ret.rows)
})

app.get('/sounds', async (req, res) => {
    var id = req.query.id
    const ret = await client.query ("SELECT * FROM sounds WHERE friend_id = " + id)
    res.status(200).send(ret.rows)
})

app.use(express.static('public'))

app.listen(3000, () => { console.log("listening to port 3000")});